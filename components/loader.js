import styled, { keyframes } from "styled-components";
import Image from 'next/image'
export default function Loader() {
    return (
        <Loading>
          <Image src="/shutdown.png" alt="" width="170" height="170"/>
        </Loading>
    )
}
const bounce = keyframes `
  0% {transform:scale(0.7)}
  50% {transform:scale(1)}
  100% {transform:scale(0.7); opacity: 0}
`
const Loading = styled.div `
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 0;
  left: 0;
  & img {
    animation: ${bounce} 1s infinite;
  }
`