import styled from "styled-components";
import Link from 'next/link'
import Typical from 'react-typical'

const Welcome = styled.div`
  /*width: 100%;
  max-width: 480px;*/
  color: white;
`
const Title = styled.h1`
  font-size: 3em;
  font-weight: 900;
  margin-bottom: -25px;
  margin-top: -20px;
`
const SubTitle = styled.div`
  font-size: 1em;
  font-weight: bold;  
`
const Line = styled.hr`
  border: 2px solid white;
`
const Button = styled.button`
  /* Adapt the colors based on primary prop */
  background: transparent;
  color: white;
  cursor: pointer;
  font-size: 1em;
  margin: 0.5em 0.25em;
  margin-left: ${props => props.first ? "0" : "0.25em"};
  margin-right: ${props => props.last ? "0" : "0.25em"};
  padding: 0.1em 1em;
  border: 3px solid white;
  /*border: none;*/
  border-radius: 15px;
  font-weight: bold;
  transition: all .5s ease;

  &:hover,&:active {
    background-color: white;
    color: teal;
  }
  @media (max-width:640px){
    padding: 0.1em 0.5em;
  }
`
const Words = [
  'Front End Development', 1000,
  '', 250,
  'ReactJS Enthusiast', 1000,
  '', 250,
  'Website Development', 1000,
  '', 250,
  'Responsive Layout', 1000,
  '', 250
]

export default function Homepage() {
    return (
        <>
        <Welcome>
            <h1>Panggil Saja</h1>
            <Title>ACILE</Title>
            <SubTitle>
              <Typical steps={Words} loop={Infinity} />
            </SubTitle>
            <Line />
            <Link href="/profil" passHref>
            <Button first>Profil</Button>
            </Link>
            <Link href="/proyek" passHref>
            <Button >Proyek</Button>
            </Link>
            <Link href="/blog" passHref>
            <Button last>Blog</Button>
            </Link>
          </Welcome>
        </>
    )
}