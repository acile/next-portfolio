import styled from 'styled-components'
import Image from 'next/image'

const Main = styled.div`
    display: flex;
    justify-content: center;

    & img {
        width: 100%;
        height: 100%;
        max-width: 450px;
        max-height: 450px;
    }

    @media (max-width:640px){
        & img {
            width: 300px;
        }
    }
`
export default function Cronstruction() {
    return (
        <>

            <Main>
                <Image src="/maintenance.svg" alt="maintenance" width="500px" height="500px" />

            </Main>
            <p style={{ textAlign: 'center' }}>Maaf, halaman yang anda tuju sedang dalam perbaikan.</p>

        </>
    )
}