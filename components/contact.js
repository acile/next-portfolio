import React, { useState } from 'react';
import axios from 'axios';
import styled from 'styled-components'
import Modal from './modal'
import Image from 'next/image'

const Btn = styled.button`
    background-color: DodgerBlue;
    color: #ffffff;
    z-index: 1000;
    position: absolute;
    
    text-align: center;
    bottom: 15px;
    right: 25px;
    cursor: pointer;
    border: none;
    border-radius: 100% 100% 20% 100%;
    width: 50px;
    height: 50px;
    box-shadow: 6px 3px 6px rgba(0, 0, 0, 0.25);
    transition: all .5s ease;
    & img {
        vertical-align: middle;
        width: calc(100% - 10px);
        height: calc(100% - 10px);
    }

    @media (max-width:720px){
        right: 15px;
    }

    &:hover, &:active{
        transform: scale(1.12);
    }
`
const InputText = styled.input.attrs({
    type: 'text'
})`
    width: 100%;
    padding: 10px;
    font-size: 16px;
    border: 1px solid #ccc;
    border-radius: 15px;
    box-sizing: border-box;
    margin-bottom: 6px;
    resize: vertical;

    &:focus {
        outline: none;
    }
`
const InputMail = styled.input.attrs({
    type: 'email'
})`
    width: 100%;
    padding: 10px;
    font-size: 16px;
    border: 1px solid #ccc;
    border-radius: 15px;
    box-sizing: border-box;
    margin-bottom: 6px;
    resize: vertical;

    &:focus {
        outline: none;
    }
`
const InputTextArea = styled.textarea`
    width: 100%;
    padding: 10px;
    font-size: 16px;
    border: 1px solid #ccc;
    border-radius: 15px;
    box-sizing: border-box;
    margin-top: 6px;
    margin-bottom: 16px;
    resize: vertical;

    &:focus {
        outline: none;
    }
`
const BtnSubmit = styled.button`
    background-color: #04AA6D;
    color: white;
    padding: 4px 20px;
    border: none;
    border-radius: 15px;
    cursor: pointer;
    width: 100%;
    font-weight: bold;
    font-size: 1.25em;

    &:hover {
        background-color: #45a049;
    }
`
export default function ContactBtn() {
    const [showModal, setShowModal] = useState(false);
    const [status, setStatus] = useState({
        submitted: false,
        submitting: false,
        info: { error: false, msg: null },
    });
    const [inputs, setInputs] = useState({
        name: '',
        email: '',
        message: '',
    });
    const handleServerResponse = (ok, msg) => {
        if (ok) {
            setStatus({
                submitted: true,
                submitting: false,
                info: { error: false, msg: msg },
            });
            setInputs({
                name: '',
                email: '',
                message: '',
            });
        } else {
            setStatus({
                info: { error: true, msg: msg },
            });
        }
    };
    const handleOnChange = (e) => {
        e.persist();
        setInputs((prev) => ({
            ...prev,
            [e.target.id]: e.target.value,
        }));
        setStatus({
            submitted: false,
            submitting: false,
            info: { error: false, msg: null },
        });
    };
    const handleOnSubmit = (e) => {
        e.preventDefault();
        setStatus((prevStatus) => ({ ...prevStatus, submitting: true }));
        axios({
            method: 'POST',
            url: 'https://formspree.io/f/mvodwwzd',
            data: inputs,
        })
            .then((response) => {
                handleServerResponse(
                    true,
                    'Terimakasih, Pesan anda telas terkirim.',
                );
            })
            .catch((error) => {
                handleServerResponse(false, error.response.data.error);
            });
    };

    return (
        <>
            <Btn onClick={() => setShowModal(true)}>
                <Image src="/admail.svg" alt="@" width="60px" height="60px" />
            </Btn>
            <Modal
                onClose={() => setShowModal(false)}
                show={showModal}
                Titled={"Hubungi Saya"}
            >
                <form onSubmit={handleOnSubmit}>
                    <label htmlFor="name">Nama :</label>
                    <InputText id="name" onChange={handleOnChange} value={inputs.name} name="_name" placeholder="Nama Anda.." required />

                    <label htmlFor="email">Email :</label>
                    <InputMail id="email" onChange={handleOnChange} value={inputs.email} name="_replyto" placeholder="Email Anda.." required />

                    <label htmlFor="message">Pesan :</label>
                    <InputTextArea id="message" onChange={handleOnChange} value={inputs.message} name="message" placeholder="Tulis Pesan Anda..." style={{ height: '150px' }} required />

                    <BtnSubmit type="submit" disabled={status.submitting}>
                        {!status.submitting
                            ? !status.submitted
                                ? 'Kirim Pesan'
                                : 'Pesan Terkirim'
                            : 'Mengirimkan...'}
                    </BtnSubmit>
                </form>
                {status.info.error && (
                    <div style={{color:'pink'}}>
                        Error: {status.info.msg}
                        <p style={{color:'whitesmoke'}}>Mohon maaf, ada sedikit gangguan...</p>
                        <p style={{color:'whitesmoke'}}>Silahkan langsung menghubungi Email: <b>arief.fajri191@gmail.com</b></p>
                    </div>
                )}
            </Modal>
        </>
    )
}