import Head from "next/head";

export default function HeadComponent({ children }) {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="teal" />
        <meta
          name="keywords"
          content="HTML, CSS, JavaScript, Front End, Web, Development, Design"
        />
        <meta name="author" content="Acile"></meta>
        {children}
      </Head>
    </>
  );
}
