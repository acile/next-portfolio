import React, { useEffect, useRef, useState } from "react";
import styled from 'styled-components'
import { BiX, BiMenu } from 'react-icons/bi'
import Link from 'next/link'
import { useRouter } from 'next/router'

const Wraper = styled.nav`
    position: fixed;
    top: 0;
    left: 0;
    z-index: 999;
    background-color: teal;
    height: 50px;
    width: 100%;
    box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.15);
`
const Navbar = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 50px;
    width: 100%;
    margin: 0 auto;
    padding: 0 30px;

    @media (max-width:640px){
        padding: 0 10px;
    }
`
const NavLogo = styled.div`
    color: #fff;
    cursor: pointer;
    display: flex;
    align-items: center;
    text-decoration: none;
    font-size: 2.5rem;
    font-weight: 800;
    transition: all .5s ease;
    &:hover{
        transform: scale(1.08);
    }
`
const MenuIcon = styled.div`
    display: none;
    @media (max-width: 640px) {
        display: block;
        position: absolute;
        top: 0;
        right: 0;
        transform: translate(-20%, 0);
        font-size: 3rem;
        color: white;
        font-weight: bold;
        cursor: pointer;
    }
`
const Menu = styled.ul`
    display: flex;
    align-items: center;
    text-align: center;
    @media only screen and (max-width:640px) {
        display: flex;
        flex-direction: column;
        width: 100%;
        height: 100vh;
        position: absolute;
        top: 50px;
        right: ${({ click }) => click ? '0' : '-100%'};
        background-color: rgba(0, 0, 0, 0.9);
        transition: all .5s ease;
        padding-top: 1em;
    }
`
const MenuItem = styled.li`
    display: flex;
    align-items: center;
    list-style: none;
    height: 50px;
    justify-content: center;

    @media (max-width:640px){
        width: 100%;
        margin: 0.2em 0;
        &:hover {
            border: none;
        }
    }

    & .active {
        background: white;
        color: teal;
    }
`
const Button = styled.button`
  /* Adapt the colors based on primary prop */
  background: transparent;
  color: white;
  cursor: pointer;
  font-size: 0.75em;
  margin: 0 0.2em;
  margin-left: ${props => props.first ? "0" : "0.2em"};
  margin-right: ${props => props.last ? "0" : "0.2em"};
  padding: 0.1em 1em;
  border: 2px solid white;
  /*border: none;*/
  border-radius: 15px;
  font-weight: bold;
  transition: all .5s ease;

  &:hover,&:active {
    background-color: white;
    color: teal;
  }
  @media (max-width:640px){
    padding: 0.5em 0.5em;
    width: 80%;
  }
`
const Navigation = () => {
    const [click, setClick] = useState(false);
    const handleClick = () => setClick(!click);
    const closeMenu = () => setClick(false);
    const router = useRouter();

    return (
        <>
            <Wraper>
                <Navbar>
                    <Link href="/" passHref>
                        <NavLogo>ACILE</NavLogo>
                    </Link>
                    <MenuIcon onClick={handleClick}>
                        {click ? <BiX /> : <BiMenu />}
                    </MenuIcon>
                    <Menu onClick={handleClick} click={click}>
                        <MenuItem>
                            <Link href="/" onClick={closeMenu} passHref>
                                <Button first >Beranda</Button>
                            </Link>
                        </MenuItem>
                        <MenuItem>
                            <Link href="/profil" onClick={closeMenu} passHref>
                                <Button className={router.pathname == "/profil" ? "active" : ""}>Profil</Button>
                            </Link>
                        </MenuItem>
                        <MenuItem>
                            <Link href="/proyek" onClick={closeMenu} passHref>
                                <Button className={router.pathname == "/proyek" ? "active" : ""}>Proyek</Button>
                            </Link>
                        </MenuItem>
                        <MenuItem>
                            <Link href="/blog" onClick={closeMenu} passHref>
                                <Button last className={router.pathname == "/blog" ? "active" : ""}>Blog</Button>
                            </Link>
                        </MenuItem>
                    </Menu>

                </Navbar>
            </Wraper>
        </>
    )
}
export default Navigation;