import Navigation from './navbar'
import styled from 'styled-components'
import ContactBtn from './contact'

const Wraper = styled.div`
  position: absolute;
  background-color: white;
  height: 100%;
  overflow-y: auto;
  overflow-x: hidden;
  margin: 0 auto;
  width: 100%;
  min-height: calc(100% - 50px);
  height: calc(100% - 50px);
  top: 50px;
`
const Main = styled.section`
  padding: 30px 20px 0;
  margin-bottom: 0;

  @media (max-width:860px){
    padding: 30px 10px 0;
}
`
export default function Layout({children}) {
    return (
        <>
            <ContactBtn />
            <Wraper>
                <Navigation />
                <Main>
                    {children}
                </Main>
                <p style={{textAlign:'center', fontSize:'.75em', margin:'10px 0', color:'gray'}}>ⓒ2021_ACILE</p>
            </Wraper>
        </>
    )
}