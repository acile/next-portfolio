import React, { useEffect, useRef, useState } from "react";
import ReactDOM from "react-dom";
import styled, { keyframes } from "styled-components";
import Image from "next/image";

const Modal = ({ show, onClose, children, Titled }) => {
    const [isBrowser, setIsBrowser] = useState(false);
    const modalWrapperRef = React.useRef();
    const backDropHandler = (e) => {
        if (!modalWrapperRef?.current?.contains(e.target)) {
            onClose();
        }
      }
  
  useEffect(() => {
    setIsBrowser(true);
  }, []);
  const handleCloseClick = (e) => {
    e.preventDefault();
    onClose();
  };
  const modalContent = show ? (
    <StyledModalOverlay onClick={backDropHandler}>
        <StyledModalWrapper ref={modalWrapperRef}>
      <StyledModal>
        <StyledModalHeader>
          <a href="#" onClick={handleCloseClick}>
            <Image src="/ui-close.svg" alt="X" width="30px" height="30px" style={{color:'white', fontWeight:'bold'}} />
          </a>
        </StyledModalHeader>
        <StyledModalTitle>{Titled}</StyledModalTitle>
        <hr />
        <StyledModalBody>{children}</StyledModalBody>
      </StyledModal>
      </StyledModalWrapper>
    </StyledModalOverlay>
  ) : null;
  if (isBrowser) {
    return ReactDOM.createPortal(
      modalContent,
      document.getElementById("modal-root")
    );
  } else {
    return null;
  }
};

const ModalKeyframe = keyframes`
    from { opacity: 0;}
    to   { opacity: 1;}
`
const StyledModalOverlay = styled.div`
  background-color: rgba(35, 55, 75, 0.9);
  z-index: 1000;
  position: absolute;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  animation: ${ModalKeyframe} .5s ease;
`;
const StyledModalHeader = styled.div`
  display: flex;
  justify-content: flex-end;
  font-size: 36px;
  font-weight: bold;
  margin-right: 15px;

  & a {
    color: white;
  }
`;
const StyledModalTitle = styled.h3`
  margin-top: -52.5px;
  margin-left: 15px;
`;
const StyledModalWrapper = styled.div`
    width: 90%;
  max-width: 500px;
  height: 90%;
    `;
const StyledModal = styled.div`
  color: white;
  width: 100%;
  height: 100%;
  border-radius: 15px;
  overflow-y: auto;
`;
const StyledModalBody = styled.div`
  padding: 15px;
  padding-top: 10px;
`;
export default Modal;
