---
title: "Cara Mudah Membuat Animasi Gelembung Full Color"
date: "12-07-2021"
desc: "Gelembung dengan warna, ukuran, dan posisi acak hanya dengan javascript dan css"
categories:
  - Belajar Coding
tags:
  - css
  - javascript
---
Membuat animasi gelembung berbagai warna seperti yang bisa kita lihat pada *website* ini sangatlah mudah. kita hanya perlu sedikit pengetahuan css dan javascript.

Baiklah agar tidak terlalu panjang, mari kita langsung mulai Belajar Coding.

## Membuat Class CSS

Untuk membuat animasi gelembung, yang pertama kali kita buat adalah objek gelembung itu sendiri. gelembung yang akan kita buat adalah berupa lingkaran sederhana. Berikut adalah kode css yang harus dibuat :

```css
.ball {
  position: absolute;
  border-radius: 100%;
  opacity: 0.3;
  z-index: -100;
}
```

Sederhana bukan ?

Disini saya memberi nama gelembung dengan class `.ball`yang didalamnya terdapat beberapa property seperti :

1. __position__ 

   Berhubung nantinya kita akan membuat efek animasi yang mengijinkan gelembung tersebut dapat bergerak kemana saja, maka kita atur nilai *position* menjadi *__absolute__*.

2. __border-radius__

   Karena bentuk yang kita inginkan adalah lingkaran sempurna, maka kita buat `border-radius` bernilai ***100%***.

3. __opacity__

   Seperti yang terlihat pada *website* ini, setiap gelembung memiliki warna yang berbeda-beda. Jadi supaya tidak menggangu objek lain kita atur `opacity` menjadi sedikit transparan, disini saya membuatnya menjadi ***0.3***.

4. __z-index__

   Sebenarnya menggunakan `z-index` adalah *opsional*. Tetapi karena saya ingin menjadikan animasi gelembung ini sebagai *background* maka saya menambahkan `z-index:-100` untuk memastikan gelembung tersebut berada dibawah semua elemen visual lainnya.

## Menambahkan Fungsi Javascript

Setelah kita membuat objek lingkaran dengan css, sekarang kita perlu membuat objek tersebut menjadi berlipat ganda dengan warna dan ukuran yang berbeda-beda. Semua elemen tersebut akan dibuat secara otomatis oleh javascript, berikut caranya :

### Tentukan Warna dan Jumlah Gelembung

Pertama kita perlu menentukan warna apa saja yang akan ditampilkan serta jumlah gelembung yang akan dibuat. 

```javascript
// membuat daftar warna 
const colors = ["#3CC157", "#2AA7FF", "#1B1B1B", "#FCBC0F", "#F85F36"]; 
// menentukan jumlah gelembung
const numBalls = 50;
```

### Menggandakan Gelembung

Selanjutnya kita buat sebuah fungsi perulangan untuk membuat gelembung dengan berbagai ukuran serta warna dan posisi yang berdeda pula. Berikut kode yang harus ditulis :

```javascript
// membuat fungsi array kosong untuk membungkus semua gelembung
const balls = [];
// mulai menggandakan gelembung
for (let i = 0; i < numBalls; i++) {
  // membuat elemen div baru untuk setiap gelembung
  let ball = document.createElement("div"); 
  ball.classList.add("ball");
  // menambahkan warna pada setiap gelembung sesuai daftar warna yang sudah kita buat
  ball.style.background = colors[Math.floor(Math.random() * colors.length)];
  // menentukan posisi /lokasi setiap gelembung secara acak
  ball.style.left = `${Math.floor(Math.random() * 100)}%`;
  ball.style.top = `${Math.floor(Math.random() * 100)}%`;
  // menentukan ukuran gelembung 
  ball.style.transform = `scale(${Math.random() * 2})`;
  ball.style.width = `${Math.random() * 5}em`;
  ball.style.height = ball.style.width;

  // membungkus semua gelembung menjadi satu array
  balls.push(ball);

}
```

### Membuat Animasi Gelembung

Setelah kita menggandakan gelembung lengkap dengan ukuran, warna, dan lokasi yang berbeda, selanjutnya kita membuat efek bergerak pada gelembung tersebut. Berikut caranya :

```javascript
// memanggil dan memisahkan array balls yang sudah kita buat
balls.forEach((el, i, ra) => {
  // membuat nilai perubahan lokasi secara acak untuk setiap gelembung
  let to = {
    x: Math.random() * (i % 2 === 0 ? -11 : 11),
    y: Math.random() * 12
  };

  // menambahkan efek animasi pada setiap gelembung
  let anim = el.animate(
    [
      { transform: "translate(0, 0)" },
      { transform: `translate(${to.x}rem, ${to.y}rem)` }
    ],
    {
      duration: (Math.random() + 1) * 2000, // random duration
      direction: "alternate",
      fill: "both",
      iterations: Infinity,
      easing: "ease-in-out"
    }
  );
});
```

---

Demikian cara membuat animasi gelembung berbagai warna. namun masih ada satu hal lagi supaya animasi gelembung berbagai warna ini dapat berjalan sebagai background halaman website kita, yaitu : 

Buat satu `id` pada css dan kita beri nama *background*

```css
#background {
    position:fixed;
    top:0;
    width:100%;
    height:100%;
}
```

Selanjutnya pada fungsi perulangan `// mulai menggandakan gelembung` kita perlu menambahkan kode berikut :

```javas
document.getElementById("background").append(ball);
```

Terakhir kita tinggal memanggil `id="background"` kedalam html kita.

---

Untuk lebih mempermudah lagi, berikut ini adalah versi lengkap kode yang kita tuliskan untuk membuat background efek animasi gelembung :

```ht
<html>
<head>
	<style>
		.ball {
  			position: absolute;
  			border-radius: 100%;
  			opacity: 0.3;
  			z-index: -100;
		}
		#background {
    		position:fixed;
    		top:0;
    		width:100%;
    		height:100%;
		}
	</style>
</head>
<body>
	<div id="background"></div>
	<script>
		// membuat daftar warna 
		const colors = ["#3CC157", "#2AA7FF", "#1B1B1B", "#FCBC0F", "#F85F36"]; 
		// menentukan jumlah gelembung
		const numBalls = 50;
		
		// membuat fungsi array kosong untuk membungkus semua gelembung
		const balls = [];
		// mulai menggandakan gelembung
		for (let i = 0; i < numBalls; i++) {
  			// membuat elemen div baru untuk setiap gelembung
  			let ball = document.createElement("div"); 
  			ball.classList.add("ball");
  			// menambahkan warna pada setiap gelembung sesuai daftar warna yang sudah kita buat
  			ball.style.background = colors[Math.floor(Math.random() * colors.length)];
  			// menentukan posisi /lokasi setiap gelembung secara acak
  			ball.style.left = `${Math.floor(Math.random() * 100)}%`;
  			ball.style.top = `${Math.floor(Math.random() * 100)}%`;
  			// menentukan ukuran gelembung 
  			ball.style.transform = `scale(${Math.random() * 2})`;
  			ball.style.width = `${Math.random() * 5}em`;
  			ball.style.height = ball.style.width;

  			// membungkus semua gelembung menjadi satu array
  			balls.push(ball);
  			// menampilkan semua gelembung ke dalam html
			document.getElementById("background").append(ball);
		}
	</script>
</body>
</html>
```

---

