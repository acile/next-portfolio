---
title: "10 Tag Html Tanpa Penutup"
date: "06-07-2021"
desc: "Elemen HTML terdiri dari tag pembuka dan tag penutup. Namun, ada juga yang tidak memiliki tag penutup"
categories:
  - Kumpulan Catatan
tags:
  - html
---
Pada umumnya, penulisan setiap elemen HTML menngunakan tag pembuka dan penutup. Namun ada beberapa tag HTML yang menurut saya *cukup spesial* karena tidak memerlukan tag penutup seperti penulisan tag lainnya.

Meskipun demikian, elemen ini tetap valid dan dapat dibaca browser. Elemen yang tidak memiliki tag penutup ini juga disebut *__void element__*, karena tidak memiliki isi.

Apa saja tag-tag yang tidak memiliki penutup itu?

Berikut adalah cacatan yang saya kumpulkan…

## `<area>`

Tag `<area>` berfungsi untuk mendefinisikan area pada *image map*. *Image map* merupakan sebuah gambar yang memiliki area yang bisa diklik. Tag `<area>` selalu berada di dalam tag `<map>`.

Contoh :

```ht
<img src="https://www.w3schools.com/tags/planets.gif" width="145" height="126" alt="Planets" usemap="#planetmap">
<map name="planetmap">
  <area shape="rect" coords="0,0,82,126" alt="Sun" href="sun.htm">
  <area shape="circle" coords="90,58,3" alt="Mercury" href="mercur.htm">
  <area shape="circle" coords="124,58,8" alt="Venus" href="venus.htm">
</map>
```

## `<base>`

Tag `<base>` berfungsi untuk membuat *base URL* dan *target* untuk sebuah link relatif. 

Contoh :

```ht
<!DOCTYPE html>
<html>
<head>
  <base href="https://www.w3schools.com/img/" target="_blank">
</head>
<body>

<p><img src="stickman.gif" width="24" height="39" alt="Stickman"> - Notice that we have only specified a relative address for the image. Since we have specified a base URL in the head section, the browser will look for the image at "https://www.w3schools.com/img/stickman.gif".</p>

<p><a href="https://www.w3schools.com">W3Schools</a> - Notice that the link opens in a new window, even if it has no target="_blank" attribute. This is because the target attribute of the base element is set to "_blank".</p>

</body>
</html>
```

Gambar `stickman.gif` akan dicari pada alamat *base URL* yang diberikan pada tag `<base>`. Kemudian, link **W3Schools** akan dibuka pada jendela baru, karena pada tag `<base>` sudah ditentukan targetnya.

## `<br>`

Tag ini fungsinya untuk membuat baris baru. Contoh :

```ht
<p>
    Langit seolah-olah menangis.<br>
    Hujan waktu itu sangat berkesan.
</p>
```

## `<col>`

Tag `<col>` berfungsi untuk memberikan atribut untuk kolom pada tabel. Tag ini ditulis dalam tag `<colgroup>`. Dengan tag `<col>` ini, kita tidak harus membuat attribut untuk setiap sel tabel (`<td>`), karena sudah terwakili.

Contoh :

```ht
<table>
  <colgroup>
    <col span="2" style="background-color:red">
    <col style="background-color:yellow">
  </colgroup>
  <tr>
    <th>ISBN</th>
    <th>Title</th>
    <th>Price</th>
  </tr>
  <tr>
    <td>3476896</td>
    <td>My first HTML</td>
    <td>$53</td>
  </tr>
  <tr>
    <td>5869207</td>
    <td>My first CSS</td>
    <td>$49</td>
  </tr>
</table>
```

## `<embed>`

Tag `<embed>` berfungsi untuk membuat *container* untuk aplikasi eksternal atau konten interaktif seperti animasi flash.

Contoh :

```html
<!DOCTYPE html>
<html>
<body>

<embed src="helloworld.swf">

</body>
</html>
```

Demo: https://www.w3schools.com/tags/tryit.asp?filename=tryhtml5_embed

## `<hr>`

Tag `<hr>` fungsinya untuk membuat garis horizontal *(horizontal row)*. Contoh penggunaanya:

```ht
<h4>Waktu itu...</h4>
<hr>
<p>Tak kusangka, hari itu bakal menjadi hari terakhirku berjumpa denganmu</p>

```

## `<img>`

Tag yang satu ini pasti sudah banyak yang tahu fungsinya. Yap… benar sekali. fungsinya untuk menampilkan gambar.

Contoh :

```ht
<img src="foto-si-doi.jpg">
```

## `<input>`

Tag `<input>` fungsinya untuk membuat elemen input pada form. Tag ini memiliki atribut `type` yang akan menentukan jenis inputannya.

Contoh :

```htm
<form action="proses.php" method="POST">
    <input type="text" name="username" placeholder="Username">
    <input type="password" name="password" placeholder="Password">
    <input type="submit" value="Masuk">
</form>
```

## `<link>`

Tag `<link>` digunakan untuk mendefinisikan hubungan antara dokumen HTML dengan *resource* eksternal seperti CSS. Tag ini juga bisa digunakan untuk membuat *favicon*.

Contoh :

```ht
<head>
    <link rel="stylesheet" href="/css/style.css">
    <link rel="icon" href="favicon.png">
</head>
```

## `<meta>`

Tag `<meta>` digunakan untuk mendefinisikan *Metadata* sebuah halaman web. *Metadata* tidak akan ditampilkan pada halaman web, melainkan akan dibaca oleh mesin atau Bot (robot). Contohnya, data yang akan tampil pada mesin pencari.

Contoh :

```htm
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Kumpulan catatan petualangan belajar coding secara otodidak">
  <meta name="keywords" content="catatan,coding,belajar,otodidak,html,css,javascript">
  <meta name="author" content="miniLIB">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
```

---

