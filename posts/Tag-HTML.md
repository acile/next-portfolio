---
title: "Mengenal Tag HTML Dasar"
date: "06-07-2021"
desc: "Tag adalah sebuah penanda awalan dan akhiran dari sebuah elemen di HTML"
categories:
  - Kumpulan Catatan
tags:
  - html
---

Tag adalah sebuah penanda awalan dan akhiran dari sebuah elemen di HTML. Tag dibuat dengan kurung siku (`<...>`), lalu didalamnya berisi nama tag dan kadang juga ditambahkan dengan atribut.

Contoh: `<p>`, `<a>`, `<body>`, `<head>`, dan sebagainya.

Tag selalu ditulis berpasangan. Ada tag pembuka dan ada tag penutupnya. Namun, ada juga beberapa tag yang tidak memiliki pasangan penutup. Tag penutup ditulis dengan menambahkan garis miring (/) di depan nama tag.

Setiap tag memiliki fungsi masing-masing. Ada yang digunakan untuk membuat judul, membuat link, membuat paragraf, heading, dan lain-lain.

Dulu.. pada awal pembuatannya HTML cuma punya 18 tag. Sekarang HTML sudah punya sekitar 250 tag.

Berikut ini daftar tag-tag dasar, yang menurut saya harus diingat.

| Tag | Fungsi |
|-----|--------|
| `<html>` | untuk memulai dokumen HTML |
| `<head>` | untuk membuat bagian head |
| `<body>` | untuk membuat bagian body |
| `<h1>` | untuk membuat heading pada artikel |
| `<p>` | untuk membuat paragraf |
| `<!-- -->` | untuk membuat komentar |

## Cara Menulis Tag HTML yang Benar!
Beberapa orang kadang sering salah dalam menuliskan tag. Ada yang lupa menutup, ada yang salah mengetik namanya, dan semacamnya.

![tag HTML](/img/tag.webp)

Berikut ini beberapa saran yang perlu diikuti agar bisa menulis tag dengan benar:

### 1. Tag-tag wajib
Ada beberapa tag yang wajib ada di HTML. Tag ini harus kamu tulis.. kalau tidak, bisa jadi kode HTML-mu akan error menurut validator W3C.

Berikut ini daftar tag yang wajib ada di HTML:

* `<!DOCTYPE html>` — untuk deklarasi type dokumen;
* `<html>` — tag utama dalam HTML;
* `<head>` — untuk bagian kepala dari dokumen;
* `<body>` — untuk bagian body dari dokumen.

### 2. Gunakan Huruf Kecil
Hindari menggunakan huruf besar dalam menuliskan nama tag dan sebaiknya gunakan huruf kecil saja.

Huruf kecil lebih gampang diketik dan juga akan membuat kode HTML terlihat lebih bersih dan rapi.

Khusus untuk tag `<!DOCTYPE html>`.. ia ditulis dengan huruf besar. Sebenarnya bisa juga dengan huruf kecil dan akan valid menurut validator W3C. Tapi untuk dokumen XHTML, menggunakan DOCTYPE dengan huruf kecil akan mengakibatkan error.

### 3. Pastikan Menutup Tag dengan Benar
Tag HTML nantinya akan ditulis bertumpuk-tumpuk. Artinya, di dalam tag ada tag lagi. Kadang kita sering salah dalam menutup tag yang bertumpuk ini. Akibatnya, kode HTML kita tidak valid.

Jika ingin tahu lebih jauh tentang tag HTML selain yang sudah disebutkan diatas, silahkan kunjungi halaman web referensi dibawah ini.

---

