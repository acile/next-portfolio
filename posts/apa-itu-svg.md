---
title: "Kelebihan Menggunakan Svg"
date: "01-07-2021"
desc: "SVG adalah format file gambar yang dapat meringankan website Anda karena ukurannya yang kecil"
categories:
  - Serba-Serbi
tags:
  - html
---

Scalable Vector Graphics (SVG) adalah format gambar yang menggunakan XML (Extensible Markup Language) sebagai dasar untuk membentuk gambar vektor dua dimensi. SVG adalah format gambar yang dikembangkan oleh World Wide Web Consortium (W3C) sejak tahun 1999. <br>Sebelum membahas tentang SVG, Anda perlu tahu bahwa ada dua golongan file gambar, yaitu raster dan vector.

Gambar raster, seperti JPG dan PNG, dibentuk oleh banyak piksel warna dengan ukuran tetap. Oleh karena itu, file gambar yang termasuk dalam kategori ini akan mengalami distorsi ketika diperbesar. Sementara itu, file vector menggunakan XML atau extensible markup language sebagai dasarnya. Dengan kata lain, file vector terdiri dari kode yang bisa Anda manipulasi.

Nah, SVG adalah salah satu tipe file gambar yang termasuk dalam golongan vector. Karena tidak terbuat dari piksel, file SVG tidak akan pecah atau kabur ketika diperbesar.

## Kelebihan SVG

Penggunaan file SVG sangat dianjurkan, selain untuk mengikuti kemajuan desain website penggunaan file SVG juga dapat berpengaruh pada kecepatan rendering halaman web. Setidaknya ada tiga keuntungan penggunaan SVG, yaitu:

### Ukuran File yang Lebih Kecil
SVG pada umumnya memiliki ukuran file yang lebih kecil dibandingkan tipe gambar lainnya seperti JPG dan PNG. Berikut adalah contoh perbandingan ukuran ketiga tipe file tersebut:

- PNG: 85,1KB
- JPG: 81,4KB
- SVG: 6.1KB

Dari perbandigan di atas, bisa Anda lihat bahwa ukuran file SVG jauh lebih kecil dibandingkan JPG dan PNG. Oleh karena itu, penggunaan SVG dapat mempercepat proses render website Anda. Namun perlu diingat bahwa tidak semua grafik perlu diganti dengan tipe file ini. Misalnya, Anda bisa menggunakan PNG atau JPG untuk featured image, sedangkan gambar kecil seperti logo dan icon menggunakan SVG.

### Kualitas Terjaga Meski Diperbesar
Seperti yang telah disebutkan sebelumnya, file SVG dibentuk dengan kode dan bukan piksel. Ketika diperbesar di program manipulasi gambar, tipe file ini dapat diperbesar tanpa rusak karena distorsi.

Sifat ini sangat dibutuhkan dalam desain website saat ini, terutama dengan adanya standar responsive design.

### Bermanfaat untuk SEO

gambar SVG dapat lebih mudah ditemukan dan dimunculkan dalam hasil pencarian jika dibandingkan JPG atau PNG. Hal ini dikarenakan file SVG memungkinkan Anda untuk memasukkan kode dan teks ke dalamnya. Untuk mengoptimalkannya, Anda bisa menambahkan kode di bawah ini:

```html
<object type="image/svg+xml" data="your_image.svg">
<img src="your_image.svg" alt="This is your image alt" title="Your image title tag">
</object>
```

Lalu, gantilah “This is your image alt” dan “Your image title tag” dengan alt text dan image title yang Anda inginkan.

## Macam macam SVG

- **SVG 1.1** adalah Rekomendasi W3C dan merupakan versi terbaru dari spesifikasi lengkap.
- **SVG Tiny 1.2** adalah Rekomendasi W3C yang ditargetkan untuk perangkat mobile.

## Fungsi SVG
SVG berfungsi untuk menampilkan grafik dimensional dalam kode XML dan juga dapat mengkreasikan sebuah grafik yang terdiri dari banyak vektor yang berbeda-beda. Pada dasarnya, SVG dapat digunakan untuk membuat tiga jenis objek grafik, yaitu :
1. Path, terdiri dari garis lurus dan kurva.
1. Gambar atau bidang.
1. Teks.

---

SVG telah direkomendasikan oleh World Wide Web Consortium (W3C) untuk menampilkan grafik serta mendeskripsikan gambar 2 dimensi dalam pengembangan web yang berbasis XML. SVG memperbolehkan tiga tipe dari objek grafis, yaitu bentuk vektor grafis (misalkan jalur yang terdiri dari garis lurus dan kurva), gambar dan teks. Hasil dari SVG dapat juga interaktif dan dinamis. Animasi dapat didefinisikan dan ditimbulkan secara menempelkan elemen animasi SVG pada isi SVG) atau dengan menggunakan skripting. SVG dapat digunakan untuk menghasilkan berbagai macam variasi dari objek grafis, dan juga menyediakan bentuk dasar umum seperti bujur sangkar dan elips. SVG memberikan pengendalian kualitas melalui sistem koordinat dari objek grafis yang telah didefinisikan dan transformasi yang akan digunakan selama proses render.

Penyimpanan berkas (file) SVG dilakukan dengan cara memberi nama ekstensinya dengan “.svg” (memakai huruf kecil semua), dan untuk menyimpan file SVG yang terkompresi memakai ekstensi “.svgz” (semua memakai huruf kecil).

---

