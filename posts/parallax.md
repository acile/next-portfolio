---
title: "Cara Mudah Membuat Efek Parallax"
date: "11-07-2021"
desc: "Untuk membuat UX design-nya lebih interaktif, kebanyakan orang menggunakan parallax effect supaya user betah berlama-lama di website"
categories:
  - Belajar Coding
tags:
  - html
  - css
---
Pada awalnya, orang-orang menggunakan efek parallax hanya karena mereka bisa, tetapi seiring waktu web designer mulai menggunakannya sebagai alat untuk menciptakan pengalaman pengguna yang lebih baik sehingga dapat membantu menarik perhatian ke konten yang berbeda.

Nah, sebenarnya apa sih, *parallax* itu? dan bagaimana cara membuatnya ? 

## Apa Itu Parallax Website?

*parallax* *website* adalah desain dengan fitur yang membuat tampilan halaman mengalami transisi saat pengunjung melakukan *scrolling* dengan *mouse.* Dalam praktiknya, *website* yang menggunakan *parallax effect* akan menampilkan halaman yang terlihat seperti dua dimensi. Jadi, secara otomatis gambar akan bergerak seiring *user* melakukan *scrolling*. Untuk membuat UX *design*-nya lebih interaktif, kebanyakan orang menggunakan *parallax effect* supaya *user* betah berlama-lama di *website*.

## Cara Membuat Parallax

Untruk membuat efek *paralax* pada website sebenarnya cukup menggunakan CSS dengan memanfaatkan property `background-attachment`. Berikut adalah contoh kode untuk membuat efek *paralax* :

``` csss
.parallax {
  /* Menggunakan gambar sebagai background */
  background-image: url("img_parallax.jpg");

  /* atur tinggi secara spesifik */
  height: 100%;

  /* Buat efek parallax */
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
```

Selanjutnya kita tinggal memanggil nama *class* tersebut kedalam Tag HTML.

```html
<div class="parallax"></div>
```

Untuk membuat efek parallax menjadi lebih menyenangkan kita bisa membuat beberapa *background-image* yang berbeda dalam satu halaman, sehingga saat pengunjung melakukan *scrolling* menuju sub-content yang berbeda maka *background* akan berubah sendiri. Berikut contoh kode untuk membuatnya :

```css
.bgimg-1, .bgimg-2, .bgimg-3 {
  position: relative;
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
.bgimg-1 {
  background-image: url("img_parallax1.jpg");
  height: 100%;
}
.bgimg-2 {
  background-image: url("img_parallax2.jpg");
  height: 100%;
}
.bgimg-3 {
  background-image: url("img_parallax3.jpg");
  height: 100%;
}
```

 Cara menngunakannya dalam HTML adalah sebagai berikut :

```html
<div class="bgimg-1">
  Tulis Caption atau Paragraf Disini
</div>
<div class="bgimg-2">
  Tulis Caption atau Paragraf Disini
</div>
<div class="bgimg-3">
  Tulis Caption atau Paragraf Disini
</div>
```

**PERHATIAN !** Beberapa perangkat seluler bermasalah dengan `background-attachment: fixed`. Namun, kita dapat menggunakan kueri media untuk menonaktifkan efek parallax untuk perangkat seluler:

```css
@media only screen and (max-device-width: 1366px) {
  .parallax {
    background-attachment: scroll;
  }
}
```

## Kelebihan dan Kekurangan Parallax

Tak bisa dimungkiri, *parallax website* adalah solusi bagi *web desainer* yang ingin memperlihatkan tampilan *website* yang ciamik kepada *user*. Namun demikian, ternyata ada kelebihan dan kekurangan saat menerapkan konsep desain *scrolling* seperti ini.

### Kelebihan

1. Terlihat Interaktif

   Bagaimana tidak, di dalamnya terdapat tampilan visual yang luar biasa sekaligus mengagumkan bagi *user* yang masuk ke dalamnya. Hal tersebut memungkinkan kamu untuk mengurangi *bounce rate* dari *website*-mu.

2. Berbeda dari yang lain

   Saat menerapkan *parallax*, otomatis tampilan *website*-mu akan berbeda dari yang lain, bahkan kompetitor sekaligus. Hal ini tentu berpotensi untuk menambah jangkauan *user*. Sebab, tampilan *website* menggunakan *parallax* dapat menarik perhatian banyak orang, terutama *user* dari kompetitor.

### Kekurangan

1. Tidak SEO-Friendly

   Salah satu konsekuensi ketika masang *parallax website* adalah tidak SEO-*Friendly*. Situs web dengan *parallax* sangat sulit untuk dioptimalkan SEO-nya. Pasalnya, kita akan kehilangan *metadata* dan *tag.* 

2. Loading jadi lambat

   Selain tidak SEO-*friendly*, menggunakan *parallax* juga membuat *website* kita akan mengalami *loading* yang lambat. Sebab, saat menerapkan *parallax*, kita akan menampilkan gambar dengan ukuran yang cukup besar sehingga berpotensi membuat *website* kita lama saat dibuka.

---

