---
title: "Memahami HTML Event Attribute"
date: "03-07-2021"
desc: "Event Attribute adalah sebuah atribut HTML yang digunakan untuk menjalankan perintah script yang secara umum dapat digunakan pada semua elemen HTML"
categories:
  - Kumpulan Catatan
tags:
  - html
---

*Event Attribute* adalah sebuah atribut HTML yang digunakan untuk menjalankan perintah *script* yang secara umum dapat digunakan pada semua elemen HTML. Perintah *script* tersebut biasanya berkaitan dengan *javascript* sebagai perintahnya.

Seperti halnya penulisan *Global Attribute* maupun Atribut HTML lainnya, dalam penulisan kode *Event Attribute* juga menggunakan ***Name*** *(nama atribut)* dan ***Value*** *(nilai dari atribut)*.

*Event* sendiri berarti peristiwa, dimana peristiwa tersebut melekat pada elemen HTML baik itu ketika ada interaksi dengan pengguna maupun berjalan secara otomatis ketika halaman HTML tersebut dimuat.

Berikut adalah beberapa **HTML Event Attribute** yang dapat disematkan dalam tag HTML:

- __onabort__ → Instruksi untuk menjalankan perintah menggagalkan perintah dari suatu elemen
- __onafterprint__ → Instruksi untuk menjalankan perintah ketika dokumen/halaman selesai dicetak *(print)*
- __onbeforeprint__ → Kebalikan dari perintah *onafterprint*
- __onblur__ → Menginstruksikan sebuah peristiwa dimana sebuah elemen kehilangan fokus
- __onchange__ → Menginstruksikan sebuah peristiwa ketika nilai/isi dari elemen berubah
- __onclick__ → Menginstruksikan sebuah peristiwa ketika sebuah elemen disentuh (klik)
- __ondbclick__ → Menginstruksikan sebuah peristiwa ketika sebuah elemen disentuh sebanyak dua kali (*double click*)
- __onscroll__ → Menginstruksikan sebuah peristiwa kepada suatu elemen ketika *scrollbar* digulung (keatas atau kebawah)
- __onresize__ → Mengintruksikan sebuah peristiwa ketika jendela browser diperbesar atau diperkecil

Demikian beberapa atribut yang sering kita temui dalam sebuah tag HTML. Tentu dengan perkembangan HTML yang semakin maju, masih ada beberapa *Event Attribute* lainnya dengan berbegai fungsi yang sangat bermanfaat untuk menjadikan halaman HTML kita lebih interaktif.

---

