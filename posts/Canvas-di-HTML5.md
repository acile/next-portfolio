---
title: "Menggunakan Atribut Canvas Di HTML5"
date: "07-07-2021"
desc: "Element canvas pada HTML digunakan untuk menggambar grafik/bitmap/teks secara interaktif menggunakan script (javascript)"
categories:
  - Kumpulan Catatan
tags:
  - html
---

Element `<canvas>` pada HTML digunakan untuk menggambar grafik/bitmap/teks secara interaktif menggunakan script (javascript). Element *canvas* hanya bertugas sebagai container (penampung), sedangkan gambar, teks, maupun grafiknya dibuat oleh script secara terpisah.

## Sejarah 
Sebelum kedatangan HTML5, Game dan elemen multimedia lainnya disajikan dalam browser menggunakan Flash. Banyak game berbasis Flash yang bisa dimainkan melalui browser. Bahkan Flash juga digunakan untuk memutar video. Saat ini HTML5 telah membawa banyak elemen baru yang menggantikan tugas Flash dalam browser. Salah satunya adalah *canvas*.

*Canvas* awalnya diperkenalkan oleh Apple untuk digunakan dalam komponen WebKit milik OS Mac X  pada tahun 2004 untuk menggerakkan aplikasi seperti widget Dashboard dan browser Safari. Kemudian, pada tahun 2005 *canvas* diadopsi dalam versi 1,8 browser Gecko, dan browser Opera mengadopsinya pada tahun 2006, lalu distandarisasi oleh Kelompok Kerja Teknologi Aplikasi Web Hypertext (WHATWG) pada spesifikasi yang diusulkan untuk teknologi web generasi berikutnya.

## Penggunaan
Canvas dapat dibuat dengan tag `<canvas>`, dengan atribut utama berupa ***Width*** (untuk mengatur lebar canvas) dan ***Height***(untuk mengatur tinggi canvas). Selain itu `<canvas>` juga mendukung *Global Attribute*, yang berarti tag tersebut dapat disisipkan semua attributes yang termasuk dalam global attributes dan secara umum berlaku untuk semua HTML tags.

Selain atribut utama dan Global Attribute, `<canvas>` juga mendukung Event Attribute. Hal ini berarti attribute tersebut dapat dijalankan ketika ada interaksi dari user atau dalam suatu peristiwa (kejadian).

Contoh :

```html
<canvas id="kanvasGambar" height="200" width="300">
<!-- Teks ini muncul apabila browser tidak mendukung HTML5 canvas -->
<p>Browser Anda tidak mendukung HTML5 Canvas</p>
</canvas>

<script>
var kanvas = document.getElementById("kanvasGambar");
var konteks = kanvas.getContext("2d");
konteks.fillStyle = "rgb(255,0,0)";
konteks.fillRect(10,50,150,50);
konteks.fillStyle = "rgb(255,255,255)";
konteks.fillRect(10,100,150,50);
</script>
```

---

