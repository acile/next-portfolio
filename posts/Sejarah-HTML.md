---
title: "Sejarah Perkembangan HTML"
date: "04-07-2021"
desc: "Setiap website mempunyai halaman HTML berbeda-beda. Masing-masing HTML terdiri dari tags atau element yang mengacu pada halaman website building block"
categories:
  - Serba-Serbi
tags:
  - html
---

Setiap website mempunyai halaman HTML berbeda-beda. Masing-masing HTML terdiri dari tags atau element yang mengacu pada halaman website building block. Elemen atau tag tersebut dapat membuat dan menyusun konten sehingga menjadi bentuk paragraf, heading, dan konten lainnya.

HTML dibuat oleh seorang ahli fisika dengan tim Berners Lee pada lembaga penelitian CERN yang ada di Swiss. Tim tersebut mempunyai ide mengenai sistem hypertext yang menggunakan basis internet. Tim Berners Lee mengeluarkan versi HTML yang pertama pada tahun 1991. Dalam versi tersebut terdapat 18 HTML tag. Hypertext merujuk pada teks yang terdapat referensi atau link untuk teks lain sehingga dapat diakses oleh pengguna.

HTML dikembangkan oleh W3C dengan upgrade yang besar pada tahun 2014. Hasilnya yaitu pengenalan tentang HTML 5 dengan semantic baru sehingga dapat memberitahukan arti dari konten HTML tersendiri. Adanya web browser bertujuan untuk memudahkan pengguna saat membuka dokumen yang berformat HTML.

## Perkembangan Kode HTML
Berikut merupakan sejarah perkembangan kode HTML mulai dari versi awal sampai sekarang.

### HTML Versi 1.0
Bermula pada versi 1.0. Versi 1.0 ini merupakan pionir yang masih banyak kelemahan sehingga tampilannya masih sederhana. HTML versi 1.0 mempunyai kemampuan untuk membuat paragraf, heading, list, hypertext dan cetak tebal atau miring pada teks. Versi 1.0 juga dapat mendukung peletakan gambar atau image pada dokumen tanpa wrapping.

### HTML Versi 2.0

HTML versi 2.0 mempunyai fitur tambahan yaitu kualitas HTML lebih baik. Versi 2.0 mempunyai kemampuan dalam menampilkan data atau form pada dokumen serta memasukan alamat, nama, dan saran atau kritik. HTML versi 2.0 ini adalah pionir untuk web interaktif.

### HTML Versi 3.0
Versi 3.0 mempunyai tambahan fitur dengan fasilitas baru yaitu figure. Figure adalah perkembangan dari image yang berfungsi untuk meletakan gambar serta tabel. HTML versi 3.0 juga dapat mendukung adanya rumus matematika pada dokumen. Versi 3.0 sekarang digantikan dengan versi 3.2.

### HTML Versi 3.2
Versi 3.2 adalah HTML yang paling sering digunakan. Versi ini mempunyai teknologi untuk meletakan teks pada keliling gambar. Sehingga gambar mempunyai latar belakang, frame, style, tabel, dan sebagainya. Versi 3.2 juga dapat menggunakan script untuk mendukung kinerja HTML.

### HTML Versi 4.0

HTML versi 4.0 mengalami banyak perubahan dari versi sebelumnya. Perubahan tersebut pada perintah HTML yaitu image, tabel, text, link, form dan sebagainya.

### HTML Versi 4.01
Terdapat juga versi 4.01 yang merupakan perbaikan dari versi 4.0. Versi ini menjadi standar untuk lemen atau atribut pada HTML. Karena sudah memperbaiki kesalahan pada versi sebelumnya.

### HTML Versi 5.0
HTML versi 5 merupakan prosedur pembuatan tampilan web terbaru dengan penggabungan antara CSS, HTML dan JavaScript. Versi ini dibuat dari informasi bahwa W3C dan IETF membuat versi HTML yang terbaru.

---



