---
title: "Mengenal Javascript Dasar"
date: "07-07-2021"
desc: "Secara Umum Javascript adalah sebuah bahasa pemrograman yang digunakan untuk membuat sebuah web menjadi lebih interaktif dan dinamis"
categories:
  - Serba-Serbi
tags:
  - javascript
---

Secara Umum Javascript adalah sebuah bahasa pemrograman yang digunakan untuk membuat sebuah web menjadi lebih interaktif dan dinamis. Menurut Wikipedia JavaScript adalah bahasa skrip yang populer di internet dan dapat bekerja di sebagian besar penjelajah web (Browser) seperti Internet Explorer (IE), Mozilla Firefox, Opera, Google Crhome dan lain-lain.

## Sejarah Javascript
JavaScript pertama kali diciptakan oleh Brendan Eich, seorang karyawan Netscape, pada tahun 1995. Netscape kala itu merupakan perusahaan software ternama yang dikenal dengan web browser miliknya, Netscape Navigator. 

Pada saat itu, bahasa pemrograman yang ia buat diberi nama Mocha dan hanya dibuat dalam waktu 10 hari karena waktu yang mepet dengan peluncuran Netscape Navigator versi 2. Brendan Eich pada awalnya diminta untuk membuat bahasa scripting seperti Java namun dapat diterapkan untuk browser. Ia pun mendesain bahasa pemrograman baru dengan menggunakan fitur-fitur yang terinspirasi dari Java, Scheme dan Self.  Nama Mocha sempat mengalami perubahan menjadi LiveScript. 

Netscape kemudian bekerja sama dengan Sun (saat ini Oracle) yang kala itu memegang lisensi Java. Kerja sama itu membawa Netscape mengubah nama LiveScript menjadi JavaScript dengan alasan kebutuhan marketing. Karena kesuksesan Javascript 1.0, Kemudian Netscape mengembangkannya menjadi Javascript 1.1 pada Web Browser Netscape Navigator, dan membawa Netscape Navigator menjadi Web Browser Terbaik pada saat itu.

Beberapa bulan kemudian Microsoft meluncurkan memperkenalkan web browser Internet Explorer 3 dengan JScript. JScript merupakan nama lain dari javascript yang dikeluarkan oleh Microsoft, hal tersebut dilakukan karena Javascript pada saat itu merupakan merk dagang yang dimiliki oleh Sun Microsystems dan Netscape, sehingga Microsoft terpaksa melakukan hal tersebut.

Penerapan JScript pada web browser Internet Explorer 3, membuat bingung para programmers, karena pada saat itu terdapat 2 versi Javascript, yaitu JavaScript di Netscape Navigator and JScript pada Internet Explorer. Javascript yang terdapat di Netscape Navigator memiliki 2 versi yaitu versi 1.0 dan 1.1, hal tersebut membuat programmer semakin bingung. Karena ketiga versi javascript tersebut memiliki fitur yang berbeda.

Untuk mengatasi masalah tersebut para programmers sepakat untuk melakukan standarisasi pada Javascript. Hingga pada pertengahan tahun 1997, Javascript 1.1 diajukan ke badan standarisasi Eropa, yaitu European Computer Manufacturers Association (ECMA) untuk membuatkan sebuah bahasa standar untuk pemrograman script pada web browser. Atas dasar itu, terbentuklah sebuah komite yang beranggotakan programmers dari berbagai macam perusahaan internet, yaitu Microsoft, Netscape, Sun Microsystem, Borlands dan beberapa perusahaan lainnya yang tertarik dalam perkembangan Javascript.

Komite Standarisasi tersebut menghasilkan sebuah bahasa pemrograman ECMAScript atau bisa disebut dengan ECMAScript-262. Satu tahun kemudian ISO (International Organization for Standardization) mengadopsi juga ECMAScript sebagai bahasa pemrograman standar. Sejak saat itulah semua web browser menjadikan ECMAScript sebagai standar acuan untuk Javascript.

## Kelebihan Javascript
Terdapat sejumlah kelebihan JavaScript yang menjadikan bahasa pemrograman ini lebih unggul daripada kompetitornya, terutama di beberapa kasus tertentu. Berikut beberapa kelebihan JavaScript:

1. kita tidak membutuhkan compiler karena web browser mampu menginterpretasikannya dengan HTML.
2. Lebih mudah dipelajari daripada bahasa pemrograman lainnya.
3. Proses pencarian dan penanganan eror atau kesalahannya lebih mudah.
4. Bisa berfungsi sebagai elemen halaman web atau event tertentu, misalnya klik atau mouseover.
5. Bisa berfungsi di berbagai browser, platform, dan lain-lain.
6. Kita bisa menggunakan bahasa pemrograman ini untuk memvalidasi input dan meminimalkan proses untuk memeriksa data secara manual.
7. website kita menjadi lebih interaktif dan juga mampu menarik perhatian lebih banyak pengunjung.
8. Lebih cepat dan ringan daripada bahasa pemrograman lainnya.

## Kekurangan Javascript
Setiap bahasa pemrograman pasti memiliki beberapa kekurangan. Salah satu penyebabnya adalah semakin populernya bahasa pemrograman yang Anda pilih, termasuk dalam hal ini adalah JavaScript. Popularitas tersebut, sialnya, mengundang para hacker, scammer, dan pihak ketiga berbahaya lainnya untuk mencari celah keamanan. Beberapa kekurangan JavaScript di antaranya:

1. Berisiko tinggi terhadap eksploitasi.
2. Bisa disalahgunakan untuk mengaktifkan kode berbahaya di komputer pengguna.
3. Tidak semua browser dan perangkat selalu mendukung bahasa pemrograman ini.
4. JS code snippet agak banyak.
5. Bisa di-render (ditampilkan) secara berbeda pada masing-masing perangkat yang malah mengarah ke inkonsistensi.

## cara Kerja Javascript

Biasanya, kita bisa menyematkan JavaScript langsung ke halaman website atau mengarahkannya melalui file .js sendiri. JavaScript merupakan bahasa client-side, yang berarti proses pengunduhan dan pemrosesan script terjadi di perangkat pengunjung/pengguna. Sedangkan untuk bahasa pemrograman server-side, proses tersebut terjadi pada server sebelum bahasa pemrograman mengirimkan file ke pengunjung situs.

> Perlu diketahui, sebagian web browser juga menawarkan kesempatan bagi user untuk menonaktifkan JavaScript. Jadi, sebaiknya cari tahu apa yang terjadi pada event yang pengguna unduh ke perangkat yang bahkan tidak mendukungnya.

## Perbedaan Javascript dengan Bahasa Pemrograman Lainnya
Alasan mengapa JavaScript menjadi salah satu bahasa pemrograman yang populer adalah kemudahan proses belajar dan penggunaannya. Banyak developer yang bahkan akhirnya memilih JavaScript sebagai bahasa pemrograman terbaik. Bahasa pemrograman lainnya hanya diperlukan jika developer menginginkan sesuatu yang lebih spesifik.

Berikut beberapa bahasa pemrograman yang paling populer:

- **Javascript**

    Fungsi JavaScript, atau yang sering disingkat JS, adalah menjadikan website lebih interaktif. Script bahasa pemrograman ini berjalan di browser, bukan server, dan biasanya masuk ke library pihak ketiga untuk menyediakan fungsionalitas tingkat lanjut tanpa mengharuskan developer melakukan coding dari awal.

- **CSS**

    CSS adalah singkatan dari Cascading Style Sheets. Dengan CSS, para webmaster bisa mengatur style serta menentukan tampilan setiap jenis konten. Untuk menampilkan konten, Anda bisa melakukannya secara manual terhadap setiap elemen dalam HTML. Namun, jika Anda melakukannya di CSS, Anda tidak perlu berulang kali menentukan tampilan elemen-elemen tersebut setiap kali akan Anda gunakan.

- **HTML**

    Merupakan singkatan dari Hypertext Markup Languange, dan menjadi salah satu bahasa pemrograman yang paling umum di web serta membentuk buildin block  dari suatu halaman situs. Sebagai contoh, tag HTML yang termasuk `<p>` untuk paragraf dan `<img>` untuk gambar.

- **PHP**

    PHP merupakan bahasa pemrograman server-side, sedangkan JavaScript client-side. Bahasa ini sering terlihat di sistem manajemen konten berbasis PHP, misalnya WordPress, tapi juga sering digunakan untuk pengembangan back-end karena menyediakan jalan terbaik guna memfasilitasi proses transfer informasi dari dan ke database.

---

Ibarat membangun sebuah rumah. HTML adalah struktur bangunan rumah dan sandaran bagi pintu dan tembok, sedangkan CSS adalah karpet dan wallpaper yang menghiasi setiap sudut rumah sehingga tampak lebih indah dan menarik. JavaScript, dalam hal ini, menambahkan sifat responsif sehingga Anda bisa membuka pintu dan menyalakan lampu. Anda bisa saja tidak menggunakan lampu atau tidak membutuhkan pintu. Namun, risikonya adalah rumah yang gelap dan usang, layaknya bangunan yang telah berdiri sejak tahun 1995. Hal ini sama seperti ketika Anda memanfaatkan JavaScript untuk webiste.

---

