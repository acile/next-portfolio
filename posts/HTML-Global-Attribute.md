---
title: "Memahami HTML Global Atribut"
date: "05-07-2021"
desc: "Global Attribute adalah atribut HTML yang secara umum dapat digunakan pada semua elemen HTML"
categories:
  - Kumpulan Catatan
tags:
  - html
---

*Global Attribute* adalah atribut HTML yang secara umum dapat digunakan pada semua elemen HTML, meskipun atribut atribut tersebut tidak memiliki peranan (pengaruh) terhadap beberapa elemen.

*Global Attribute* juga bisa digunakan pada sebuah elemen yang bukan standar. Artinya, elemen yang tidak sesuai dengan standar tetap dapat menggunakan atribut global ini walaupun tidak valid. Sebagai contoh, ketika kita menuliskan `<elemenku>` jelas sekali elemen ini tidak dapat digunakan karena merupakan elemen HTML yang tidak valid. Akan tetapi, jika kita menuliskan `<elemenku hidden> hai semua...</elemenku>` maka tulisan tersebut akan disembunyikan. Atribut **hidden** merupakan salah satu dari *HTML Global Attribute* yang digunakan untuk menyembunyikan elemen.

## Macam Global Attribute
Ada beberapa atribut global dalam HTML, diantaranya:
### accesskey
Digunakan untuk menentukan *shortcut key* (tombol pintas) pada *keyboard* untuk mengaktifkan elemen tertentu sehingga halaman HTML lebih interaktif.
### contenteditable
Digunakan untuk menentukan sebuah elemen HTML apakah memperbolehkan pengguna mengubah konten didalamnya atau tidak.
### contextmenu
Digunakan untuk memberikan menu konteks pada sebuah elemen ketika ada interaksi dari pengguna *(user)*. 
### data-*
Digunakan untuk menyisipkan nama atribut yang dapat kita tentukan sendiri setelah kata "data-". nama atribut tidak boleh mengandung huruf kapital dan jarak (spasi). Atribut ini biasa digunakan untuk elemen yang akan dieksekusi oleh javascript.

### lang

Digunakan untuk menentukan bahasa apa yang ditulis pada sebuah konten didalam elemen.
### dir
Digunakan untuk menentukan arah tulisan konten/teks pada sebuah elemen. sebagai contoh, dalam Bahasa Indonesia sebuah kata/kalimat ditulis dari kiri ke kanan. Sedangkan dalam Bahasa Arab ditulis dari kanan ke kiri. Nilai yang ditetapkan dalam *dir* adalah :

- **rtl** *(right-ti-left)* yaitu penulisan konten dari kanan ke kiri.
- **ltr** *(left-to-right)* yaitu penulisan konten dari kiri ke kanan.

### draggable

Digunakan untuk menentukan apakah sebuah elemen tersebut dapat diseret dan dipindahkan atau tidak.
### dropzone
Digunakan untuk menentukan jenis konten apa yang dapat ditimpakan pada sebuah elemen. adapun *value*-nya adalah sebagai berikut:

- **copy**, menunjukkan tindakan *droping element* yang menghasilkan salinan *(copy)* elemen.
- **move**, menunjukkan elemen yang diseret akan dipindahkan ke lokasi elemen tersebut.
- **link**, akan menghasilkan sebuah *link*/tautan yang akan merujuk pada data yang di seret.

### hidden
Digunakan untuk menyembunikan sebuah elemen. Elemen yang disembunyikan tersebut dimaksudkan karena sudah tidak relevan dengan konten sekitarnya namun elemen tersebut masih dibutuhkan untuk melengkapi elemen yang lain.
### spellcheck
Digunakan untuk menentukan apakah sebuah teks harus dilakukan pengecekan ejaan dan tata bahasa *(grammar)*-nya atau tidak. Secara *default*, pada sebuah *textarea*, browser akan mengecek ejaan tulisan. Jika salah maka ditandai dengan garis bawah berwarna merah.
### tabindex
Digunakan untuk menentukan urutan penggunaan `tab` pada sebuah papan tombol *(keyboard)* dalam mengakses sebuah elemen. Biasanya atribut *tabindex* digunakan untuk membuat formulir.
### translate
Digunakan untuk menentukan apakah sebuah konten dari elemen tersebut diterjemahkan *(translate)* atau tidak.

---

Penulisan *Global Attribute* sama dengan menulis atribut pada umumnya seperti yang telah kita bahas pada artikel tag HTML dan Atribut HTML. Meski begitu, ada beberapa *Global Attribute* yang dapat ditulis **hanya nama atributnya** saja. Seperti atribut *hidden* dapat ditulis cukup dengan menyebutkan namanya saja atau lengkap dengan *value*-nya.

---

