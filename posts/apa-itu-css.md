---
title: "Sejarah dan Perkembangan CSS"
date: "08-07-2021"
desc: "CSS meruapakan sebuah bahasa untuk mengatur tampilan web sehingga terlihat lebih menarik dan indah"
categories:
  - Serba-Serbi
tags:
  - css
---
CSS adalah singkatan dari ***Cascading Style Sheet***, merupakan kumpulan perintah yang disusun menurut urutan tertentu untuk mengatur komponen dalam suatu halaman web. CSS seperti halnya *styles* pada aplikasi *Microsoft Word* yang dapat mengatur beberapa gaya, misalnya heading, bodytext, footer, images dan gaya lainnya untuk dapat dipakai bersama-sama dalam beberapa file. CSS biasanya digunakan untuk memformat tampilan web yang dibuat dengan bahasa HTML dan XHTML. CSS mampu mengatur warna body teks, ukuran gambar, ukuran border, warna mouse over, warna tabel, warna hyperlink, margin, spasi paragraph, spasi teks dan parameter lainnya.

## Sejarah CSS
Nama CSS didapat dari fakta bahwa setiap deklarasi *style* yang berbeda dapat diletakkan secara berurutan, yang kemudian membentuk hubungan ayah-anak (parent-child) pada setiap style. CSS sendiri merupakan sebuah teknologi internet yang direkomendasikan oleh *World Wide Web Consortium* atau W3C pada tahun 1996. Setelah CSS distandardisasikan, Internet Explorer dan Netscape melepas browser terbaru mereka yang telah sesuai atau paling tidak hampir mendekati dengan standar CSS.

Format dasar CSS yang banyak digunakan sekarang ini merupakan ide dari seorang programmer bernama ***Hakon Wium Lie*** yang tertuang dalam proposalnya mengenai Cascading HTML Style Sheet (CHSS) pada bulan oktober 1994 (dalam konferensi W3C di Chacigo. Illinois). Beliau bersama dengan Bert Bos mengembangkan suatu standart CSS dan Pada tahun 1996, CSS resmi dipublikasikan. Pengerjaan Proyek ini juga didukung oleh seorang progamer Thomas Reardon dari perusahaan software ternama, Microsoft. 

## Perkembangan CSS
Saat ini terdapat tiga versi CSS, yaitu CSS1 dikembangkan berpusat pada pemformatan dokumen HTML, CSS2 dikembangkan untuk memenuhi kebutuhan terhadap format dokumen agar bisa ditampilkan di printer, sedangkan CSS3 adalah versi terbaru dari CSS yang mampu melakukan banyak hal dalam desain website. Berikut adalah uraian sejarah singkatnya:

### CSS1
Pada tanggal 17 Agustus 1996 World Wide Web Consortium (W3C) menetapkan CSS sebagai bahasa pemrograman standard dalam pembuatan web. Tujuannya adalah untuk mengurangi pembuatan tag-tag baru oleh Netscape dan Internet Explorer, karena kedua browser tersebut sedang bersaing mengembangkan tag sendiri untuk mengatur tampilan web.

CSS 1 mendukung pengaturan tampilan dalam hal :

1. Font (Jenis ketebalan).
2. Warna, teks, background dan elemen lainnya.
3. Text attributes, misalnya spasi antar baris, kata dan  huruf.
4. Posisi teks, gambar, table dan elemen lainnya.
5. Margin, border dan padiing.

### CSS2

Pada tahun 1998, W3C menyempurnakan CSS tahap awal dengan menciptakan standard CSS2 yang menjadi standard hingga saat ini. Pada level CSS2 ini, dimasukkan semua atribut dari CSS1 dan diperluas dengan penekanan pada *International Accessibiality and Capacibility* kususnya media-specific CSS. CSS 2 dikembangkan untuk memenuhi kebutuhan terhadap format dokumen agar bisa ditampilkan pada printer. 

### CSS3 
CSS3 adalah versi terbaru dari perkembangan CSS yang mampu melakukan banyak hal dalam mendesain website. CSS3 dapat melakukan animasi pada halaman website, diantaranya animasi warna dan animasi 3D. Dengan CSS 3 desaigner dimudahkan dalam hal kompatibilitas websitenya pada smartphone dengan dukungan fitur baru yakni media query. Selain itu, banyak fitur baru pada CSS 3 yaitu : Multiple background, border-radius, drop-shadow, border-image, CSS-Math dan CSS Object Model.
Fitur terbaru CSS 3 :

1. Animasi, sehingga pembuatan animasi tidak memerlukan program sejenis Adobe Flash dan Microsoft Silverlight.
2. Beberapa efek teks, seperti teks berbayang, kolom koran dan "Word-Wrap".
3. Beberapa efek pada kotak, seperti kotak yang ukurannya dapat diubah-ubah, transformasi 2 dimensi dan 2 dimensi, sudut-sudut yang tumpul dan bayangan.

---

