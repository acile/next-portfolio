---
title: "Membuat Menu Navigasi Sederhana"
date: "09-07-2021"
desc: "Memiliki menu navigasi yang mudah digunakan sangat penting untuk situs web mana pun"
categories:
  - Belajar Coding
tags:
  - html
  - css
---

Memiliki menu navigasi yang mudah digunakan sangat penting untuk situs web mana pun. Menu navigasi ibarat papan penunjuk arah pada sebuah bangunan yang menunjukan jalan kepada pengunjung menuju tempat tertentu. Hal ini biasa kita jumpai di tempat-tempat fasilitas umum, gedung perkantoran, dan sebagainya.

Pada kesempatan kali ini, saya akan menuliskan bagaimana cara membuat menu navigasi sederhana hanya dengan HTML dan CSS saja tanpa menggunakan JavaScript.

## Menu Navigasi Standar

Sebuah menu navigasi yang sering kita lihat dalam setiap halaman website sebenarnya adalah kumpulan link. Jadi untuk membuatnya kita perlu menyiapkan tag HTML yang berupa *list* (daftar) menggunakan `<ul>` dan `<li>` terlebih dahulu. 

Contoh :

```html
<ul>
  <li><a href="#home">Home</a></li>
  <li><a href="#news">News</a></li>
  <li><a href="#contact">Contact</a></li>
  <li><a href="#about">About</a></li>
</ul>
```

hasilnya akan seperti ini :

![menu-1](/img/menu1.png)

Selanjutnya kita hilangkan icon *bullet* dan jarak disebelah kiri dengan cara menuliskan kode CSS sebagai berikut :

```css
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
```

maka akan menjadi seperti ini :

![menu-1](/img/menu2.png)

Penjelasan kode :

- `list-style-type: none;` berfungsi untuk menghapus icon *bullet* karena menu navigasi kita tidak membutuhkan penanda daftar/list
- `margin` dan `padding` diatur dengan nilai 0 untuk menghapus pengaturan *default* browser sehingga setiap item menu yang kita buat tidak memiliki jarak/spasi di sebelah kirinya.

Kode pada contoh di atas adalah kode standar yang digunakan untuk membuat menu navigasi, dari sini kita bisa mengkreasikan tampilan menu navigasi website sesuai keinginan kita. Lalu bagaimana caranya memodifikasi tampilan menu navigasi ? berikut sedikit contoh yang bisa dilakukan.

## Menu Navigasi Vertikal

Untuk membuat menu navigasi vertikal, kita dapat mengatur gaya elemen  `<a>` pada kode diatas :

```css
li a {
  display: block; /*wajib*/
  width: 160px; /*wajib*/
  text-decoration: none; /*wajib*/
  color: #000;
  padding: 8px 16px;
  background-color: #f1f1f1;
}
```

hasilnya akan seperti ini :

![menu-1](/img/menu3.png)

dari beberapa baris kode diatas, yang perlu diperhatikan adalah dua kode yang  diberi komentar `/*wajib*/` , berikut penjelasanya :

- baris kode `display: block` berfungsi untuk menampilkan tautan/link sebagai elemen blok dan membuat seluruh area dapat di klik (bukan hanya bagian teks saja).
- baris kode `width:160px` digunakan untuk mengatur lebar setiap item menu menjadi 160px.
- baris kode `teks-decoration:none` digunakan untuk menghilangkan garis bawah pada teks, sehingga tampiulan menu kita menjadi lebih bagus.
- kode `color:#000` untuk mengatur warna huruf menjadi hitam
- kode `background-color:#f1f1f1` untuk mengatur warna latar belakangnya.
- dan yang terakhir `paddin: 8px 16px` untuk mengatur jarak atas dan samping kiri teks.

## Menu Navigasi Horizontal

Cara membuat menu navigasi horizontal tidak jauh beda dengan membuat menu navigasi vertikal, hanya saja kita perlu menambahkan sedikit baris kode untuk mengatur gaya pada elemen `<li>`. Ada dua cara untuk membuatnya, yaitu dengan menambahkan kode `display:inline` atau bisa juga dengan `float: left`.  saya sendirio lebihb senang menggunakan cara kedua, yaitu dengan menambahkan `float:left` karna lebih mudah dimodifikasi nantinya  hehe...

Contoh :

```css
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}

li {
  float: left;
}

li a {
  display: block; /*wajib*/
  width: 160px; /*wajib*/
  text-decoration: none; /*wajib*/
  color: #000;
  padding: 8px 16px;
  background-color: #f1f1f1;
}
```

hasilnya akan seperti ini :

![menu-1](/img/menu4.png)

## Kesimpulan

Membuat menu navigasi pada sebuah halaman website sebenarnya sangatlah mudah. kita hanya perlu sedikit pengetahuan tentang HTML dan CSS saja sudah bisa membuat menu navigasi yang dibutuhkan.

Sebenarnya masih banyak lagi trik untuk membuat menu navigasi website yang bisa digunakan. Seperti mengubah warna tombol saat kursor berada diatasnya, mengubah posisi menu, dan lain sebagainya. Kedepan kita akan menuliskan semua di website ini

---

