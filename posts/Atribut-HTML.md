---
title: "Mengenal Atribut HTML Dasar"
date: "08-07-2021"
desc: "Atribut adalah bagian penting dari HTML. Atribut berfungsi untuk memberikan informasi tambahan tentang isi elemen"
categories:
  - Kumpulan Catatan
tags:
  - html
---

Atribut adalah bagian penting dari HTML. Atribut berfungsi untuk memberikan informasi tambahan tentang isi elemen. Mereka muncul pada *tag pembuka* dan terdiri dari dua bagian, yaitu: **Name** (nama atribut) dan **Value** (nilai atribut itu sendiri) yang dipisahkan oleh tanda sama dengan (=).

**Value** (Nilai) adalah informasi yang ditentukan untuk *atribute*. Harus ditulis diantara tanda petik (baik tunggal maupun ganda). Penulisan atribut yang sama pada tag HTML yang berbeda dapat memiliki *value* yang berbeda pula.

Tanda kutip ganda adalah yang paling umum digunakan dalam HTML, namun penggunaan tanda kutip tunggal juga dapat digunakan. Dalam beberapa situasi, ketika nilai atribut itu sendiri berisi tanda kutip ganda, maka perlu menggunakan tanda kutip tunggal sebagai pembungkusnya, negitu juga sebaliknya.

Contoh:
```html
<p title='ini "Web" minilib' >
<p title="ini 'cara' sebaliknya" >
```
Penulisan atribut dan nilai atribut bersifat *case-insensitive* yang berarti tidak dipengaruhi huruf besar maupun huruf kecil. Namun, *World Wide Web Consortium (W3C)* merekomendasikan untuk penulisan atribut/nilainya ditulis menggunakan huruf kecil dalam rekomendasi HTML4 mereka.

## Atribut yang Sering Digunakan
Perkembangan HTML sampai saat ini sudah cukup canggih, begitu pula dengan jumlah atribut yang dapat digunakan dalam *syntax* HTML. Namun ada beberapa atribut yang sering digunakan dalam elemen HTML, berikut diantaranya:

### title
Atribut *title* berfungsi untuk memberikan judul yang disarankan untuk sebuah elemen. Perilaku atribut ini akan bergantung pada *elemen* yang membawanya. Atribut *title* sering ditampilkan sebagai *tooltip (keterangan/informasi)* ketika kursor muncul diatas elemen atau saat elemen sedang dimuat.

### href

Atribut *href* biasa digunakan pada tag `<a>` untuk menyisipkan sebuah *link*/tautan alamat web pada tulisan. 

contoh:
```html
<a href="http://www.minilib.xyz/"> ini adalah link</a>
```

### style
Atribut *style* memungkinkan kita untuk menentukan/menambahkan aturan *Cascading Style Sheet (CSS)* dalam elemen.

contoh:
```html
<p style="color: #000000; background-color: yellow; font-size:16px; font-weight: bold; text-align: center;"> 
Ini Teks Yang Kita Buat Dengan Menambahkan Atribut Style Didalamnya
</p>
```
Hasilnya akan seperti ini :
> <p style="color: #000000; background-color: yellow; font-size:16px; font-weight: bold; text-align:center;"> Ini Teks Yang Kita Buat Dengan Menambahkan Atribut Style Didalamnya</p>

### alt
Atribut *alt* biasanya digunakan untuk memberikan gambaran singkat mengenai suatu gambar atau tulisan. Atribut **alt** dianggap penting karena beberapa hal, yaitu:

- BOT yang hanya memiliki kemampuan untuk membaca informasi teks.
- Jika gambar gagal ditampilkan saat loading, maka atribut ini memegang peran penting sebagai informasi mengenai gambar.

### id
Atribut **id** dari tag HTML dapat digunakan untuk mengidentifikasi secara unik setiap elemen didalam halaman HTML. Ada dua alasan utama mengapa kita mungkin ingin menggunakan atribut **id** pada elemen:

- Jika sebebuah elemen membawa atribut *id* sebagai pengidentifikasi unik yang hanya menampilkan sesuatu secara khusus.
- Jika kita memiliki dua elemen dengan nama yang sama dalam halaman Web, kita dapat menggunakan atribut *id* untuk membedakan tampilan antara element tersebut, baik dengan *style sheet* maupun javascript.

### class
Atribut *class* digunakan untuk mengaitkan elemen HTML dengan *style sheet* dan menentukan *class element*. Nilai atribut juga dapat berupa daftar nama *class* yang dipisahkan dengan spasi.

---

Selain beberapa atribut yang sudah kita bahas diatas, masih ada beberapa atribut yang dapat kita gunakan dalam tag HTML. Ada atribut yang dapat digunakan untuk semua elemen (*Global Attribute*), ada pula atribut yang digunakan untuk menjalankan suatu perintah (*Event Attribute*). Bahkan ada juag atribut yang dapat kita buat sendiri.

---

