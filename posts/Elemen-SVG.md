---
title: "Berkenalan dengan Elemen SVG"
date: "05-07-2021"
desc: "SVG singkatan dari Scalable Vector Graphics adalah format gambar yang menggunakan XML"
categories:
  - Kumpulan Catatan
tags:
  - html
---

SVG singkatan dari Scalable Vector Graphics adalah format gambar yang menggunakan XML (eXtensible Markup Language) sebagai dasar untuk membentuk gambar vektor dua dimensi. SVG merupakan format gambar yang dikembangkan oleh Word Wide Web Consortium (W3C) sejak tahun 1999.

Menggambar SVG dalam HTML dapat menggunakan tag elemen `<svg></svg>`. Tag tersebut dapat diatur dimensi dan posisinya menggunakan atribut viewbox. Nilai Atribut dari viewbox ini memiliki 4 buah nilai, yaitu min-x, min-y, width dan height, menentukan dimensi dan posisi SVG ini dapat disesuaikan dengan kebutuhan.

## Elemen Dasar
sebelum mulai membuat gambar dengan file svg, akan lebih baik jika kita mengetahui beberapa elemen dasar yang dapat digunakan untuk menggambar sebuah objek/gambar. berikut diantaranya:

1. `<svg>` Digunakan untuk membungkus dan mendefinisikan seluruh grafik. `<svg>` adalah tag html yang khusus digunakan untuk membuat gambar vektor di halaman web.
2. `<line>` Digunakan untuk membuat satu garis lurus.
3. `<polyline` Digunakan untuk membuat garis multi segmen.
4. `<rect>` Digunakan untuk membuat persegi panjang.
5. `<ellipse>` digunakan untuk membuat lingkaran atau oval.
6. `<polygon>` Digunakan untuk membuat bentuk sisi lurus, dengan tiga sisi atau lebih.
7. `<path>` Digunakan untuk membuat bentuk apapun yang kita suka dengan mendefinisikan poin dan garis antara mereka.
8. `<defs>` Digunakan untuk mendefinisikan aset-aset yang dapat digunakan kembali.
9. `<g>` Digunakan untuk membungkus beberapa bentuk menjadi satu kelompok (grouping).
10. `<symbol>` Seperti sebuah grup, namun dengan beberapa fitur tambahan. Biasanya ditempatkan di bagian `<defs>`.
11. `<use>` Membawa aset yang didefinisikan dalam bagian `<defs>` dan membuat mereka terlihat di SVG

## Bentuk Dasar

SVG menetapkan enam bentuk dasar, termasuk juga path dan teks, yang dapat digabungkan untuk membentuk sebuah gambar grafis. Setiap bentuk ini mempunyai properti yang menjelaskan posisi dan ukuran dari bentuk. Warna dan garis ditentukan oleh properti fill dan stroke.

### Elips
Elemen untuk membangun sebuah elips menggunakan elemen `<ellipse>` dimana terdapat atribut rx yang mendefinisikan jari-jari horisontal dan ry yang mendefinisikan jari-jari vertikal. Contoh:
```html
<ellipse cx=”40” cy=”40” rx=”35” ry=”25”/>
```

### Persegi empat
Elemen untuk membangun segi empat menggunakan `<rect>` dimana terdapat atribut rx dan ry untuk mendefinisikan ujung bulat dari bujur sangkar. Jika ujungnya tidak bulat maka rx=ry=0 atau tidak ada atribut tersebut. Contoh:
```html
<rect x=”40” y=”40” width=”75” height=”100” rx=”30” ry=”20”/>
```
atau bisa juga ditulis seperti ini:
```
<rect x=”40” y=”40” width=”75” height=”100”/>
```

### Garis

Menampilkan garis diantara dua koordinat dan untuk membangunnya menggunakan elemen `<line>`. Contoh:
```html
<line x1=”0” y1=”150” x2=”400” y2=”150”/>
```

### Banyak garis (Polyline)
Menampilkan sebuah rangkaian garis dengan vertek yang sudah ditentukan dan untuk membangunnya menggunakan elemen `<polyline>`. Contoh:
```html
<polyline points=”50,175 150,175 150,125 250,200”/>
```

### Poligon
Sama seperti polyline, tetapi ditambah sebuah garis dari titik terakhir ke titik yang pertama, membuatnya dan mengandung paling sedikit tiga sisi. Contoh:
```html
<polygon points=”350,75 379,175 355,175 355,200 345,200 345,175 321,175”/>
```

### Path
Path memiliki konsep menghubungkan titik ke titik lainnya. Konsep ini dapat diperluas untuk menggambar kurva-kurva atau form-form yang sangat kompleks. Path juga dapat digunakan untuk membuat animasi dan bahkan untuk mengkreasikan teks. Path dapat membuat tiga tipe kurva, antara lain:
```html
<path d=”M75,100 a50,25 0 1,0 50,25”/>
<path d=”M75,100 c25,-75 50,50 100, 0 s50,-50 150,50”/>
<path d=”M75,225 q25,-75 100,0 t150,50”/>
```

### Teks
Elemen teks dapat dipecah menjadi beberapa bagian menggunakan elemen tspan, membuat masing-masing bagian menjadi bentuk individual. Contoh:
```html
<text x=”20” y=”20” font-size=”30”>by their
<tspan fill=”rgb(255,0,0)”>R</tspan>
<tspan fill=”rgb(0,255,0)”>G</tspan>
<tspan fill=”rgb(0,0,255)”>B</tspan>
values</text>
```

---

