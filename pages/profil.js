import Layout from "../components/layout";
import HeadComponent from "../components/head";
import styled, { keyframes } from "styled-components";
import {
    SiHtml5,
    SiCss3,
    SiJavascript,
    SiPhp,
    SiPython,
    SiAdobeillustrator,
    SiAdobephotoshop,
    SiReact,
    SiGit,
} from "react-icons/si";
import { AiOutlineConsoleSql } from "react-icons/ai";

const Main = styled.div`
  display: grid;
  grid-template-columns: 55% auto;
  grid-gap: 1.5em;
  padding: 0 40px;

  @media (max-width: 760px) {
    grid-template-columns: auto;
    grid-gap: 5px;
    padding: 0 20px;
  }

  @media (max-width: 480px) {
    padding: 0 10px;
  }
`;
const FadeLeft = keyframes`
    from { opacity: 0; margin-left: -50px; }
    to   { opacity: 1; margin-left: 0px;}
`;
const FadeRight = keyframes`
    from { opacity: 0; margin-left: 50px; }
    to   { opacity: 1; margin-left: 0px;}
`;
const Description = styled.div`
  width: 100%;
  hyphens: auto;
  animation: ${FadeLeft} .5s;

  & p {
    margin-bottom: 0.7em;
    hyphens: auto;
    font-size: 1.4rem;
  }

  @media (max-width: 760px) {
    border-bottom: 10px double gray;
  }
`;
const SideKomp = styled.div`
    animation: ${FadeRight} .5s;
`
const IconGrid = styled.div`
  border-bottom: 10px double gray;
  padding: 10px;
  padding-bottom: 0;
  margin-bottom: 5px;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(90px, 1fr));

  & div {
    width: 100px;
    text-align: center;
    font-size: 0.7em;
    padding: 0 0 20px 0;
    margin: 0 auto;
    transition: all .5s ease;
  }

  & svg {
    font-size: 50px;
  }

  & div:hover {
    transform: scale(1.2);
  }
`;
const CertList = styled.div`
  padding: 0 20px;

  @media (max-width: 480px) {
    padding: 0 10px;
  }
`;
const List = styled.div`
  width: 100%;
  border: 3px solid gray;
  border-radius: 15px;
  box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.25);
  padding: 10px 15px;
  margin-top: ${(props) => (props.last ? "10px" : "0")};
  font-size: 1.5rem;
  transition: all .5s ease;

  &:hover {
    transform: scale(1.08);
  }

  & h3 {
    line-height: 1em;
  }

  & p {
    margin: 0 0 -5px 0;
  }
`;
export default function Profil() {
    return (
        <>
            <HeadComponent>
                <title>Acile | Profil</title>
                <meta
                    name="description"
                    content="pengembang web yang siap bekerja keras untuk menciptakan solusi elegan dalam waktu singkat"
                />
            </HeadComponent>
            <Layout>
                <Main>
                    <Description>
                        <h1>Haloo,..</h1>
                        <p>
                            Nama saya Arif Fajri dan biasa dipanggil <b>ACILE</b>.
                        </p>
                        <p>
                            Seorang pengembang web yang siap bekerja keras untuk menciptakan
                            solusi elegan dalam waktu singkat. Menguasai beberapa pengetahuan
                            tentang penggunaan kode pemrograman, desain/perancangan, bahasa
                            markup dan hal-hal lain yang terkait dengan sektor ini. Mampu
                            bekerja secara mandiri dan efektif dalam proyek independen, maupun
                            berkolaborasi dengan sebuah tim.
                        </p>
                        <p>
                            Saya percaya bahwa saya akan menjadi orang terbaik dalam proyek
                            anda, dan saya menunggu tanggapan positif dari anda segera. Jangan
                            pernah ragu untuk memanfaatkan keterampilan dan pengetahuan saya
                            sebaik mungkin untuk menyelesaikan proyek dengan sukses.
                        </p>
                        <p>
                            Anda dapat menemukan saya di{" "}
                            <a
                                href="https://www.linkedin.com/in/arif-fajri/"
                                target="_blank"
                                rel="noreferrer"
                            >
                                <b>LinkedIn</b>
                            </a>{" "}
                            atau langsung menghubungi saya melalui tombol disebelah kanan
                            bawah halaman ini.
                        </p>
                        <p>Terimakasih telah berkunjung…</p>
                    </Description>
                    <SideKomp>
                        <h1 style={{ textAlign: "center", textDecoration: "underline" }}>
                            .:KOMPETENSI:.
                        </h1>
                        <IconGrid>
                            <div>
                                <SiHtml5 />
                                <br />
                                <b>HTML</b>
                            </div>
                            <div>
                                <SiCss3 />
                                <br />
                                <b>CSS</b>
                            </div>
                            <div>
                                <SiJavascript />
                                <br />
                                <b>Javascript</b>
                            </div>
                            <div>
                                <SiReact />
                                <br />
                                <b>ReactJS</b>
                            </div>
                            <div>
                                <SiGit />
                                <br />
                                <b>Git</b>
                            </div>
                            <div>
                                <SiPhp />
                                <br />
                                <b>PHP</b>
                            </div>
                            <div>
                                <AiOutlineConsoleSql />
                                <br />
                                <b>SQL</b>
                            </div>
                            <div>
                                <SiAdobeillustrator />
                                <br />
                                <b>Illustrator</b>
                            </div>
                            <div>
                                <SiAdobephotoshop />
                                <br />
                                <b>Photoshop</b>
                            </div>
                        </IconGrid>
                        <h1 style={{ textAlign: "center", textDecoration: "underline" }}>
                            .:SERTIFIKAT:.
                        </h1>
                        <CertList>
                            <List>
                                <h3>Responsive Web Design</h3>
                                <p>FreeCodeCamp </p>
                                <a
                                    style={{ fontWeight: "bold" }}
                                    href="https://www.freecodecamp.org/certification/acile/responsive-web-design"
                                    target="_blank"
                                    rel="noreferrer"
                                >
                                    Lihat Kredensial
                                </a>
                            </List>
                            <List last>
                                <h3>SQL (Basic)</h3>
                                <p>HackerRank </p>
                                <a
                                    style={{ fontWeight: "bold" }}
                                    href="https://www.hackerrank.com/certificates/b22dc030110a"
                                    target="_blank"
                                    rel="noreferrer"
                                >
                                    Lihat Kredensial
                                </a>
                            </List>
                        </CertList>
                    </SideKomp>
                </Main>
            </Layout>
        </>
    );
}
