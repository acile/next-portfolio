import Link from "next/link";
import Layout from '../components/layout'
import HeadComponent from '../components/head'
import { getSortedPostsData } from "../lib/posts";
import styled from 'styled-components'
import StackGrid from 'react-stack-grid'

const Item = styled.div`
    background-color: Ivory;
    color: black;
    box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.25);
    border-radius: 0.75em;
    padding-bottom: .15em;
    font-size: 1.4rem;
`
const Title = styled.div`
    padding: .7em;
    border-radius: 0.5em 0.5em 0 0;
    background-color: peru;
    cursor: pointer;
    transition: all .5s ease;

    & h3 {
        line-height: 1.2em;
        margin: 0;
        color: #ffffff;
        text-transform: uppercase;
        
    }

    &:hover, &:active {
        background-color: teal;
        transform: scale(1.04);
        box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.5);
        border-radius: 0.5em 0.5em 5px 5px;
    }
`
const Desc = styled.div`
    padding: 0 .9em;

    & p {
        margin: 10px 0;
        line-height: 1.3em;
    }
`

export async function getStaticProps() {
    const allPostsData = getSortedPostsData();
    return {
        props: {
            allPostsData,
        },
    };
}

export default function Blog({ allPostsData }) {
    return (
        <>
            <HeadComponent>
            <title>Acile | Blog</title>
            <meta name="description" content="pengembang web yang siap bekerja keras untuk menciptakan solusi elegan dalam waktu singkat" />
            </HeadComponent>
            <Layout>
                <StackGrid
                    columnWidth={250}
                    gutterWidth={20}
                    gutterHeight={20}
                    style={{marginBottom:'30px'}}
                >
                    {allPostsData.map(({ id, title, desc }, index) => (
                        <Item key={index}>
                            <Link href={`/${id}`} passHref>
                                <Title>
                                    <h3>{title}</h3>
                                </Title>
                            </Link>
                            <Desc>
                                <p>{desc}</p>
                            </Desc>
                        </Item>
                    ))}
                </StackGrid>
            </Layout>
        </>
    )
}