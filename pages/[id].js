import Layout from "../components/layout";
import { getAllPostIds, getPostData } from "../lib/posts";
import HeadComponent from '../components/head';
import styled from 'styled-components'
import ReactMarkdown from "react-markdown";

const Main = styled.div`
    margin: 0 auto;
    max-width: 800px;
    
    & h1 {
      border-bottom: 5px solid peru;
      line-height: 1.2em;
      margin-bottom: 10px;
    }
    & h2 {
      font-size: 1.2em;
    }
    & p {
      font-size: 0.9em;
      margin-bottom: 0.7em;
      hyphens: auto;
    }
    & pre {
      overflow: auto;
      background: WhiteSmoke;
      padding: 20px;
      border-radius: 10px;
      margin-bottom: 0.7em;
      font-size: 0.75em;
    }
    & hr {
      border: 2px solid gray;
      margin-bottom: 0.75em;
    }
    & ol, & ul {
      margin-left: 2em;
      margin-bottom: 1em;
    }
`
const DateShow = styled.div`
    font-size: .7em;
    margin-top: -12px;
    color: white;
    float: right;
    & div {
      background: peru;
      width: fit-content;
      padding: 0 10px 5px 10px;
      font-weight: bold;
      border-radius: 0 0 10px 10px;
    }
`
export async function getStaticProps({ params }) {
  const postData = await getPostData(params.id);
  return {
    props: {
      postData,
    },
  };
}

export async function getStaticPaths() {
  const paths = getAllPostIds();
  return {
    paths,
    fallback: false,
  };
}

export default function Post({ postData }) {
  
  return (
    <>
    <HeadComponent>
      <title>Acile | {postData.title}</title>
      <meta name="description" content={postData.desc} />
    </HeadComponent>
    <Layout>
      <Main>
        <h1 style={{textTransform: 'uppercase'}}>{postData.title}</h1>
        {/*<DateShow>
          <div>{postData.date}</div>
        </DateShow>*/}
        {/*<div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />*/}
        <ReactMarkdown>{ postData.contentHtml }</ReactMarkdown>
      </Main>
    </Layout>
    </>
  );
}