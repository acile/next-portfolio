import GlobalStyles from "../GlobalStyles";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { initGA, logPageView } from "../components/analytics";
import Loader from "../components/loader";
function MyApp({ Component, pageProps }) {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    // Loading function to load data or
    // fake it using setTimeout;
    const loadData = async () => {
      // Wait for two second
      await new Promise((r) => setTimeout(r, 2000));

      // Toggle loading state
      setLoading((loading) => !loading);
    };

    loadData();
  }, []);
  const router = useRouter();
  useEffect(() => {
    if (typeof window === undefined) return;
    const handleRouteChange = (url) => {
      logPageView(url);
    };
    if (!window.location.href.includes("localhost")) {
      if (!window.GA_INITIALIZED) {
        initGA();
        window.GA_INITIALIZED = true;
      }
      router.events.on("routeChangeComplete", handleRouteChange);
    }
    return () => {
      if (typeof window === undefined) return;
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);
  if (loading) {
    return <Loader />;
  } else {
    return (
      <>
        <GlobalStyles />
        <Component {...pageProps} />
      </>
    );
  }
}

export default MyApp;
