import Layout from "../components/layout";
import HeadComponent from "../components/head";
import styled, { keyframes } from "styled-components";

const Main = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: 800px;
`;
const CardFadeIn = keyframes`
  from { opacity: 0; margin-top: 50px; }
  to   { opacity: 1; margin-top: 0px;}
`;
const Card = styled.div`
  border-radius: 15px;
  border: 5px solid white;
  box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.5);
  color: white;
  margin-bottom: 20px;
  display: grid;
  grid-template-columns: 300px auto;

  
  opacity: 1;
  animation: ${CardFadeIn} .5s;
  transition: all .15s ease;
  @media (max-width: 640px) {
    grid-template-columns: auto;
  }
  &:hover, &:active {
    border: 5px solid teal;
  }
`;
const ImgContent = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: 700px;
  

`;
const Img = styled.img`
  width: 100%;
  height: 100%;

`;
const Content = styled.div`
  color: black;
  padding: 0.5em;
  & h2 {
    font-size: 1.2em;
  }

  & p {
    font-size: 0.9em;
    margin-bottom: 0.7em;
    hyphens: auto;
    line-height: 1.3em;
  }
`;
const Button = styled.a`
  /* Adapt the colors based on primary prop */
  background: transparent;
  color: DodgerBlue;
  cursor: pointer;
  font-size: 0.75em;
  font-weight: bold;
  padding: 0.1em 1em;
  border: 2px solid DodgerBlue;
  /*border: none;*/
  border-radius: 15px;
  font-weight: bold;
  text-decoration: none;
  transition: all .5s ease;

  &:hover,
  &:active {
    background-color: DodgerBlue;
    color: white;
    text-shadow: none;
  }
  @media (max-width: 640px) {
    padding: 0.5em 0.5em;
    width: 80%;
  }
`;
const Small = styled.div`
  color: gray;
  font-size: .75em;
  margin-top: -10px;
`
export default function Profil() {
  return (
    <>
      <HeadComponent>
        <title>Acile | Proyek</title>
        <meta
          name="description"
          content="pengembang web yang siap bekerja keras untuk menciptakan solusi elegan dalam waktu singkat"
        />
      </HeadComponent>
      <Layout>
        <Main>
          <Card>
            <ImgContent>
              <Img src="/proyek/travelinfo.png" alt="proyek-1"/>
            </ImgContent>
            <Content>
              <h2>Travelinfo</h2>
              <Small>Responsive Website | PWA | ReactJS | API | JSON</Small>
              <p>
                Merupakan proyek pribadi yang bertujuan untuk mempromosikan
                potensi wisata minat khusus di wilayah Kabupaten Wonosobo dan
                sekitarnya.
              </p>
              <Button href="https://travelinfo-cek.vercel.app" target="_blank" rel="noreferrer">Kunjungi Website</Button>
            </Content>
          </Card>
          <Card>
            <ImgContent>
              <Img src="/proyek/proyek2.png" alt="proyek-2"/>
            </ImgContent>
            <Content>
              <h2>Dapur Ibu</h2>
              <Small>Responsive Website | ReactJS | API</Small>
              <p>
                Sebuah replikasi website yang berisi resep masakan dengan berbagai kategori. Data resep diambil dari sebuah <i>Public API</i> masakapaharini.com 
              </p>
              <Button href="https://dapur-ibu.vercel.app" target="_blank" rel="noreferrer">Kunjungi Website</Button>
            </Content>
          </Card>
        </Main>
      </Layout>
    </>
  );
}
